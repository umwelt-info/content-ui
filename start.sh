#!/bin/bash -xe

composer install

if [ -f web/sites/default/files/.ht.sqlite ]
then

    vendor/bin/drush --yes updatedb

else

    vendor/bin/drush --yes site:install

    # Fix up fresh install to match the exported configuration
    vendor/bin/drush --yes entity:delete shortcut
    vendor/bin/drush --yes config-set system.site uuid 84b29e98-d9ab-4c03-964e-37f4636ab55e

fi

vendor/bin/drush --yes config:import

# Build the custom theme
pushd web/themes/custom/umweltinfo
npm install
npm run build
popd

# Integrate the search UI
ZWEIG=${ZWEIG:-main}

curl --output artifacts.zip --location https://gitlab.opencode.de/api/v4/projects/2532/jobs/artifacts/$ZWEIG/download?job=build
unzip -o artifacts.zip

mv dist/assets/index-*.js web/themes/custom/umweltinfo/dist/index_search-ui.js
mv dist/assets/index-*.css web/themes/custom/umweltinfo/dist/styles_search-ui.css

rm artifacts.zip dist -rf

vendor/bin/drush cache:rebuild

vendor/bin/drush runserver 8082
