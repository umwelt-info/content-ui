# User interface for umwelt.info content

## Getting started
- Start the project by running `./start.sh`
- This will setup a new Drupal instance, import our configuration, download the search UI and build the custom theme.
- When it has finished, visit the website locally at `http://localhost:8082/`.
- To login as the `admin` user, use `vendor/bin/drush --uri=http://localhost:8082 user:login` to generate a one time link.
- If you want to the search UI integration, run `npm run proxy` in a separate tab in the `search-ui` folder.
- When you are done or if you want to pull changes or switch branches, just stop the script by pressing Ctrl+C and restart as before when ready.

## Stashing changes
- If you want to stop your VM but keep changes which are not yet or should never be properly integrated into version control, you can stash the files of your CMS instance on your workstation.
- On your workstation, run `ssh entwicklung -- sudo tar -cpzv -C content-ui/web/sites/default/files . > content-ui.tar.gz` to retrieve all files from the VM into the compressed archive `content-ui.tar.gz` stored on your workstation.
- After recreating your VM and bringing up the CMS as usual, run `ssh entwicklung -- sudo tar -xpzv -C content-ui/web/sites/default/files < content-ui.tar.gz` on your workstaiton to put those files back onto your VM.
