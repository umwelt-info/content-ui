import infoBox from './modules/infoBox';
import mainMenu from './modules/mainMenu';
import sidebarMenu from './modules/sidebarMenu';
import sitemapAccordion from './modules/sitemapAccordion';
import searchThemeFilter from "./modules/searchThemeFilter";
import ratingBlock from './modules/ratingBlock';
import stickySidebar from "./modules/stickySidebar";
import contactBarrier from "./modules/contactBarrier.js";
import dashboardCharts from "./modules/dashboardCharts";
import taxonomyTermAccordion from "./modules/taxonomyTermAccordion";
import stickySidebarTaxonomy from "./modules/stickySidebarTaxonomy";
import accordion from "./modules/accordion.js";


(() => {
  infoBox();
  mainMenu();
  sidebarMenu();
  sitemapAccordion();
  searchThemeFilter();
  ratingBlock();
  stickySidebar();
  contactBarrier();
  dashboardCharts();
  taxonomyTermAccordion();
  stickySidebarTaxonomy();
  accordion();

  window.API_ENDPOINT = process.env.API_ENDPOINT;
})();