import { behaviors } from 'Drupal'; //eslint-disable-line
import $ from 'jquery'; //eslint-disable-line

export default () => {
  behaviors.contactBarrier= {
    attach: function (context, settings) {
      if (!once('contactBarrier', 'html').length) {
        return;
      }

      var selectInput = $('#edit-subject', context);

      if (!selectInput.length) {
        return;
      }

      if (window.location.pathname.includes('/kontakt') || window.location.pathname.includes('/contact')) {
        var urlParams = new URLSearchParams(window.location.search);
        var issueParam = urlParams.get('issues');

        if (issueParam) {
          selectInput.val(issueParam);
          selectInput.trigger('change');
        }
      }
    }
  };
};
