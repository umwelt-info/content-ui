export default function accordion() {
  document.addEventListener("DOMContentLoaded", function () {
    const accordions = document.querySelectorAll(".accordion-button");

    accordions.forEach((button) => {
      button.addEventListener("click", function () {
        const expanded = this.getAttribute("aria-expanded") === "true";
        this.setAttribute("aria-expanded", !expanded);
        console.log("toggled");

        const content = document.getElementById(
          this.getAttribute("aria-controls")
        );
        content.hidden = expanded;
      });

      button.addEventListener("keydown", function (event) {
        if (event.key === "Enter" || event.key === " ") {
          this.click();
        }
      });
    });
  });
}

