import { behaviors } from 'Drupal'; //eslint-disable-line
import $ from 'jquery'; //eslint-disable-line

export default () => {
  behaviors.mainMenu = {
    attach(context) {
      if (!once('mainMenu', 'html').length) {
        return;
      }

      const menu = $('.menu--main');
      const expandableItems = menu.find('.menu-item--has-submenu');
      const toggleVisible = (target) => {
        target.toggleClass('sub-menu--visible');
        target.toggleClass('is-active');
      };
      const removeVisible = (target) => {
        target.removeClass('sub-menu--visible');
        target.removeClass('is-active');
      };
      const collapseAll = () => {
        expandableItems.each((index, el) => {
          removeVisible($(el));
        })
      };
      const adjustSubMenuPosition = (element) => {
        const mainMenuRightBorder = menu[0].getBoundingClientRect().right;
        const elementRightBorder = element.getBoundingClientRect().right;

        if (elementRightBorder > mainMenuRightBorder) {
          $(element).addClass('align-right');
        }
      }

      expandableItems.click((e) => {
        e.stopPropagation();
        const parent = $(e.target).parent('.menu-item--has-submenu');

        if (!parent.hasClass('is-active')) {
          collapseAll();
          toggleVisible(parent.parents('.menu-item--has-submenu'));
        }
        toggleVisible(parent);

        const subMenu = parent.find('> ul.menu')[0]
        adjustSubMenuPosition(subMenu);
      });

      $(document).click((e) => {
        const target = $(e.target)
        if (target.closest('.menu-item--has-submenu').length === 0) {
          collapseAll();
        }
      });
    },
  };
};
