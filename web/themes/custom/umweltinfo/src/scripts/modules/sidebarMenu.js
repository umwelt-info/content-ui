import { behaviors } from 'Drupal'; //eslint-disable-line
import $ from 'jquery'; //eslint-disable-line

export default () => {
  behaviors.sidebarMenu = {
    attach(context) {
      if (!once('sidebarMenu', 'html').length) {
        return;
      }

      const menu = $('.menu--sidebar-menu');
      const sidebarMenuToggle = $('.sidebar-menu-toggle');
      const sidebarMenuCloseTrigger = $('.sidebar-menu-close-trigger');
      const mainMenu = $('.menu--main');

      const attachMainMenuItems = () => {
        const mainMenuItems = $('.menu--main > .menu-level-0').clone();
        const sidebarMenuItems = menu.find('> ul');

        sidebarMenuItems.before(mainMenuItems);

        const expandableItems = menu.find('.menu-item--has-submenu');
        const toggleVisible = (target) => {
          target.toggleClass('sub-menu--visible');
          target.toggleClass('is-active');
        };
        const removeVisible = (target) => {
          target.removeClass('sub-menu--visible');
          target.removeClass('is-active');
        };
        const collapseAll = () => {
          expandableItems.each((index, el) => {
            removeVisible($(el));
          })
        };

        expandableItems.click((e) => {
          e.stopPropagation();
          const parent = $(e.target).parent('.menu-item--has-submenu');

          if (!parent.hasClass('is-active')) {
            collapseAll();
            toggleVisible(parent.parents('.menu-item--has-submenu'));
          }
          toggleVisible(parent);

          const subMenu = parent.find('> ul.menu')[0];
          adjustSubMenuPosition(subMenu);
        });
      }

      attachMainMenuItems();

      sidebarMenuToggle.click((e) => {
        menu.addClass('visible');
        sidebarMenuCloseTrigger.focus();
      });

      sidebarMenuToggle.keypress((e) => {
        if (e.which === 13) {
          menu.addClass('visible');
          sidebarMenuCloseTrigger.focus();
        }
      });

      sidebarMenuCloseTrigger.click((e) => {
        sidebarMenuToggle.focus();
        menu.removeClass('visible');
      });

      sidebarMenuCloseTrigger.keypress((e) => {
        if (e.which === 13) {
          sidebarMenuToggle.focus();
          menu.removeClass('visible');
        }
      });

      $(menu).find('.menu > li:last-child').bind('focusout', (e) => {
        sidebarMenuToggle.focus();
        menu.removeClass('visible');
      });

      $(document).click((e) => {
        const target = $(e.target)
        if (menu.hasClass('visible') && !target.closest('.menu--sidebar-menu').length && !target.closest('.sidebar-menu-toggle').length) {
          menu.removeClass('visible');
        }
      });
    },
  };
};
