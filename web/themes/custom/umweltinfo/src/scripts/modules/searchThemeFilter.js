import { behaviors } from 'Drupal'; //eslint-disable-line
import $ from 'jquery'; //eslint-disable-line

export default () => {
  behaviors.searchThemeFilter = {
    attach(context) {
      const view = $('.view-article-overview');
      const viewContainer = view.parent('.views-element-container');
      const modalWindow = view.find('.view-filters');
      const modalOpenTrigger = view.find('.theme-filter-trigger');
      const modalCloseTrigger = modalWindow.find('.modal-close-trigger');
      const searchTerm = modalOpenTrigger.find('.search-term')
      const termLinks = modalWindow.find('.bef-link');
      const selectedSearchTermAttribute = 'data-selected-search-term';
      const show = () => {
        modalWindow.removeClass('hidden');
      };
      const hide = () => {
        modalWindow.addClass('hidden');
      };
      const updateSelectedSearchTerm = () => {
        if (viewContainer.attr(selectedSearchTermAttribute)) {
          searchTerm.html(viewContainer.attr(selectedSearchTermAttribute));
        }
      }

      updateSelectedSearchTerm();

      modalOpenTrigger.click(() => {
        show();
      });

      modalCloseTrigger.click(() => {
        hide();
      });

      $(document).click((e) => {
        const target = $(e.target)
        if (!target.closest('.view-filters').length && !target.closest('.theme-filter-trigger').length) {
          hide();
        }
      });

      termLinks.click((e) => {
        viewContainer.attr(selectedSearchTermAttribute, $(e.target).text());
      });
    },
  };
};
