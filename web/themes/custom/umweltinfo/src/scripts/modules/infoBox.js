import { behaviors } from 'Drupal'; //eslint-disable-line
import $ from 'jquery'; //eslint-disable-line
import Chart from 'chart.js/auto'; //eslint-disable-line

export default () => {
  behaviors.infoBox = {
    attach(context) {
      if (!once('infoBox', 'html').length) {
        return;
      }

      const infoBox = $('.info-box');

      const modalWindow = infoBox.find('.info-modal');
      const modalOpenTrigger = infoBox.find('.modal-trigger');
      const modalCloseTrigger = modalWindow.find('.modal-close-trigger');

      modalOpenTrigger.on("click", () => {
        modalWindow.addClass('show');
      });

      modalCloseTrigger.on("click", () => {
        modalWindow.removeClass('show');
      });

      $(document).on("click", (e) => {
        const target = $(e.target)
        if (!target.closest('.info-modal').length && !target.closest('.modal-trigger').length) {
          modalWindow.removeClass('show');
        }
      });

      const statistics = infoBox.find('#statistics[data-value]').get(0);
      if (!statistics) {
        return;
      }
      const statisticsValue = JSON.parse(statistics.dataset.value);

      const createBarChart = (title, labels, data) => {
        const canvas = document.createElement("canvas");
        statistics.appendChild(canvas);

        new Chart(canvas, {
          type: "bar",
          data: {
            labels,
            datasets: [
              {
                data,
                backgroundColor: "rgba(0, 149, 255, 1)",
                borderColor: "rgba(0, 125, 214, 1)",
              },
            ],
            borderWidth: 1,
          },
          options: {
            plugins: {
              title: {
                display: true,
                text: title,
                align: "start",
                padding: {
                  bottom: 16
                },
                font: {
                  family: window.getComputedStyle(canvas).fontFamily,
                  size: 19,
                  weight: 400
                }
              },
              legend: {
                display: false
              }
            }
          }
        });
      };

      if (statisticsValue.view_duration) {
        const viewDuration = [
          statisticsValue.view_duration.ten_seconds,
          statisticsValue.view_duration.one_minute,
          statisticsValue.view_duration.five_minutes,
        ];

        createBarChart("Verweildauer", ["10s", "1m", "5m"], viewDuration);
      }

      if (statisticsValue.scroll_depth) {
        const scrollDepth = [
          statisticsValue.scroll_depth.ten_percent,
          statisticsValue.scroll_depth.fifty_percent,
          statisticsValue.scroll_depth.seventyfive_percent,
        ];

        createBarChart("Scroll-Tiefe", ["10%", "50%", "75%"], scrollDepth);
      }
    },
  };
};
