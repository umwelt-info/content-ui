import { behaviors } from 'Drupal'; // eslint-disable-line
import Plotly from 'plotly.js-cartesian-dist';

export default () => {
  behaviors.plotlyPlot = {
    attach: function (context, settings) {
      if (!once('plotlyPlot', 'html').length) {
        return;
      }

      for (const plot of document.querySelectorAll('div.plot')) {
        const obj = plot.querySelector("script[type='application/json']");
        Plotly.newPlot(plot, JSON.parse(obj.textContent));
      }
    }
  };
};