import { behaviors } from 'Drupal'; //eslint-disable-line
import $ from 'jquery'; //eslint-disable-line

export default () => {
  behaviors.sitemapAccordion = {
    attach: function (context, settings) {
      let acc = once('acc-title', '.sitemap .view-header', context);

      acc.forEach(function (v, i) {
        $(v).on('click', function () {
          this.classList.toggle('closed');

          let panel = this.nextElementSibling;
          if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
          }
          else {
            panel.style.transition = 'max-height 0.3s ease-in-out';
            panel.style.maxHeight = '0';
          }
        });
      });
    }
  }
}
