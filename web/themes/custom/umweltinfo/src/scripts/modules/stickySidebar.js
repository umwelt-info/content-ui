import { behaviors } from 'Drupal'; //eslint-disable-line
import $ from 'jquery'; //eslint-disable-line
import StickySidebar from "sticky-sidebar";

export default () => {
  behaviors.stickySidebar = {
    attach(context) {
      if (!once('stickySidebar', 'html').length) {
        return;
      }

      const containerSelector = 'article.node.node--view-mode-full>.node__content>.layout--twocol-section';

      if (!$(containerSelector).length) {
        return;
      }

      let sidebar = new StickySidebar(containerSelector + '>.layout__region.layout__region--second', {
        containerSelector: containerSelector,
        topSpacing: 0,
        bottomSpacing: 10,
        minWidth: 1024,
      });
    },
  };
};
