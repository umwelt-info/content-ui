import { behaviors } from 'Drupal'; //eslint-disable-line
import $ from 'jquery'; //eslint-disable-line

export default () => {
  behaviors.taxonomyTermAccordion = {
    attach: function (context, settings) {
      let acc = once('taxonomyTermAccordion', '.field--name-field-external-sources .field__label', context);

      acc.forEach(function (v, i) {
        $(v).addClass('closed');
        $(v).next().addClass('closed');

        $(v).on('click', function () {
          $(this).toggleClass('closed');
          $(this).next().toggleClass('closed');
        });
      });
    }
  }
}
