import { behaviors } from 'Drupal'; //eslint-disable-line
import Chart from 'chart.js/auto'; //eslint-disable-line

export default () => {
  behaviors.dashboardCharts = {
    attach(context) {
      if (!once('dashboardCharts', 'html').length) {
        return;
      }

      if (!drupalSettings.dashboardBlock) {
        return;
      }

      const currentLanguage = drupalSettings.path.currentLanguage;
      const dateFormat = Intl.DateTimeFormat(currentLanguage);

      const data = drupalSettings.dashboardBlock.data;
      data.forEach((row) => {
        row.timestamp = new Date(row.timestamp);
      });

      const createLineChart = (element, config, yAxisKey) => {
        const ctx = element.getContext("2d");

        const gradient = ctx.createLinearGradient(0, 0, 0, 300);
        gradient.addColorStop(0, "rgba(0, 157, 255, 0.32)");
        gradient.addColorStop(1, "rgba(0, 149, 255, 0)");

        new Chart(element, {
          type: 'line',
          data: {
            datasets: [
              {
                label: config.labels[currentLanguage],
                data: data,
                backgroundColor: gradient,
                borderColor: "rgba(0, 125, 214, 1)",
                pointBackgroundColor: "rgba(0, 149, 255, 1)",
                fill: "origin"
              },
            ],
            borderWidth: 1
          },
          options: {
            parsing: {
              xAxisKey: "timestamp",
              yAxisKey: yAxisKey
            },
            maintainAspectRatio: false,
            scales: {
              x: {
                type: "linear",
                bounds: "data",
                ticks: {
                  callback: (date) => dateFormat.format(date),
                }
              },
              y: {
                min: parseInt(config.axis_limits.y.min) || 0,
                max: parseInt(config.axis_limits.y.max) || null,
              }
            },
            tension: 0.2,
            plugins: {
              tooltip: {
                callbacks: {
                  title: () => "",
                  label: (context) => `${dateFormat.format(context.parsed.x)}: ${context.parsed.y}`
                }
              },
              title: {
                display: true,
                text: config.labels[currentLanguage],
                align: 'start',
                padding: {
                  bottom: 16
                },
                font: {
                  family: window.getComputedStyle(element).fontFamily,
                  size: 19,
                  weight: 400
                }
              },
              legend: {
                display: false
              }
            }
          }
        });
      };

      createLineChart(
        document.getElementById('dashboard-block-chart-datasets'),
        drupalSettings.dashboardBlock.datasets_config,
        "datasets"
      );

      createLineChart(
        document.getElementById('dashboard-block-chart-sources'),
        drupalSettings.dashboardBlock.sources_config,
        "sources"
      );

      createLineChart(
        document.getElementById('dashboard-block-chart-providers'),
        drupalSettings.dashboardBlock.providers_config,
        "providers"
      );
    },
  };
};
