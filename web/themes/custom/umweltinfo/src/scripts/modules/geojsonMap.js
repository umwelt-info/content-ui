import { behaviors } from 'Drupal'; // eslint-disable-line
import Map from 'ol/Map';
import View from 'ol/View';
import Overlay from 'ol/Overlay';
import { fromLonLat } from 'ol/proj';
import { boundingExtent } from 'ol/extent';
import { Style, Text, Circle, Fill, Stroke } from 'ol/style';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import OSM from 'ol/source/OSM';
import VectorSource from 'ol/source/Vector';
import Cluster from 'ol/source/Cluster';
import GeoJSON from 'ol/format/GeoJSON';
import Popover from 'bootstrap/js/dist/popover';

export default () => {
  behaviors.geojsonMap = {
    attach: function (context, settings) {
      if (!once('geojsonMap', 'html').length) {
        return;
      }

      const anchorElement = document.getElementById('geojson-map-source');
      if (!anchorElement) {
        return;
      }

      const layers = []

      for (const key in anchorElement.dataset) {
        if (key.startsWith("layerUrl-")) {
          layers.push(anchorElement.dataset[key]);
        }
      }

      if (layers.length === 0) {
        console.error("No layers found in anchor element");
      }

      const map = new Map({
        target: 'map',
        layers: [
          new TileLayer({
            source: new OSM()
          })
        ],
        view: new View({
          center: fromLonLat([10.4515, 51.1657]),
          zoom: 6,
          maxZoom: 16,
        })
      });


      for (const layer of layers.slice(0, -1)) {
        const vectorSourceBackground = new VectorSource({
          url: layer,
          format: new GeoJSON(),
        });

        const vectorLayerBackground = new VectorLayer({
          source: vectorSourceBackground,
          style: new Style({
            fill: new Fill({ color: 'rgba(188, 209, 231, 0.25)' }),
            stroke: new Stroke({ color: 'rgb(98, 170, 242)' }),
          })
        });

        map.addLayer(vectorLayerBackground);
      }

      const vectorSource = new VectorSource({
        url: layers.at(-1),
        format: new GeoJSON()
      });

      const markerStyle = new Style({
        image: new Circle({
          radius: 8,
          fill: new Fill({ color: 'rgb(9, 0, 102)' }),
          stroke: new Stroke({
            color: 'rgb(255, 255, 255)',
            width: 2
          })
        })
      });

      const vectorLayer = new VectorLayer({
        source: vectorSource,
        minZoom: 10,
        style: markerStyle
      });

      map.addLayer(vectorLayer);

      const clusterSource = new Cluster({
        distance: 20,
        minDistance: 10,
        source: vectorSource,
      });

      const clusterImage = new Circle({
        radius: 10,
        stroke: new Stroke({
          color: 'rgb(255, 255, 255)',
        }),
        fill: new Fill({
          color: 'rgb(9, 0, 102)',
        }),
      });

      const styleCache = {};

      let vectorLayerCluster = new VectorLayer({
        source: clusterSource,
        maxZoom: 10,
        style: function (feature) {
          const size = feature.get('features').length;
          let style = styleCache[size];
          if (!style) {
            style = new Style({
              image: clusterImage,
              text: new Text({
                text: size.toString(),
                fill: new Fill({
                  color: 'rgb(255, 255, 255)',
                }),
              }),
            });
            styleCache[size] = style;
          }
          return style;
        },
      });

      map.addLayer(vectorLayerCluster);

      let popup = new Overlay({
        element: document.getElementById('popup')
      });

      map.addOverlay(popup);

      map.on('singleclick', (event) => {
        const features = map.getFeaturesAtPixel(event.pixel)

        const element = popup.getElement();

        let popover = Popover.getInstance(element);

        if (popover) {
          popover.dispose();
        }

        popup.setPosition(undefined);

        if (features.length) {
          if (features[0].getKeys().includes('popup')) {
            popover = new Popover(element, {
              container: element,
              placement: 'top',
              animation: false,
              content: features[0].getProperties()['popup'],
              html: true,
            });

            popover.show();

            popup.setPosition(event.coordinate);
            return;
          }

          const clustered = features[0].get('features')
          if (clustered && clustered.length) {
            const extent = boundingExtent(
              clustered.map((feature) => feature.getGeometry().getCoordinates()),
            );
            map.getView().fit(extent, { duration: 1000, padding: [50, 50, 50, 50] });
          }
        }
      });
    }
  };
};