import {behaviors} from 'Drupal'; //eslint-disable-line
import $ from 'jquery'; //eslint-disable-line

export default () => {
  behaviors.ratingBlock = {
    attach(context) {
      if (!once('ratingBlock', 'html', context).length) {
        return;
      }

      const onClick = (element) => {
        $(element).prevAll().addBack().addClass('full');
        $(element).nextAll().removeClass('full');

        const rating = $(element).data('value');
        const path = window.location.pathname;

        $.ajax({
          url: '/api/v1/rating-by-path_add',
          method: "POST",
          data: {
            path: path,
            rating: rating
          },
          error: (response) => {
            console.log('An error occurred during Rating request: ' + response.responseJSON.message);
          },
        });
      };

      $('body').find('.rating-block__container').each(function () {
        $(this).find('.rating-block_star')
          .click(function(e) {
            onClick(this);
          })
          .keydown(function (e) {
            if (e.keyCode === 13) {
              onClick(this);
            }
          })
          .hover(function() {
            $(this).prevAll().addBack().addClass('hover');
          }, function() {
            $(this).prevAll().addBack().removeClass('hover');
          }
        );
      });
    },
  };
};
