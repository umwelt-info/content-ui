const path = require("path");
const Dotenv = require('dotenv-webpack');
const globImporter = require("node-sass-glob-importer");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");

module.exports = (env, argv) => {
  const isDevMode = argv.mode === "development";
  return {
    mode: isDevMode ? "development" : "production",
    devtool: isDevMode ? "source-map" : false,
    entry: {
      main: ["./src/scripts/main.js", "./src/styles/main.scss"],
      maps: { import: ["./src/scripts/maps.js", "./node_modules/ol/ol.css"], dependOn: "main" },
      plots: { import: "./src/scripts/plots.js", dependOn: "main" },
    },
    module: {
      rules: [
        {
          test: /\.s?css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader
            },
            {
              loader: "css-loader",
              options: {
                sourceMap: true,
                url: false
              }
            },
            {
              loader: "postcss-loader",
              options: {
                sourceMap: true
              }
            },
            {
              loader: "sass-loader",
              options: {
                sassOptions: {
                  importer: globImporter(),
                },
                sourceMap: true,
              }
            }
          ]
        },
        {
          test: /\.js$/,
          exclude: /\/node_modules\//,
          use: {
            loader: "babel-loader",
            options: {
              presets: [["@babel/preset-env", { modules: false }]]
            }
          }
        }
      ]
    },
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, 'dist'),
      clean: true
    },
    plugins: [
      new MiniCssExtractPlugin(),
      new Dotenv(),
    ],
    externals: {
      Drupal: 'Drupal',
      jquery: 'jQuery',
    },
    optimization: {
      minimizer: [
        new TerserPlugin({
          minify: TerserPlugin.swcMinify,
        }),
        new CssMinimizerPlugin(),
      ],
    }
  };
};