<?php

namespace Drupal\umweltinfo_statistics\Client;

use Drupal;
use GuzzleHttp\Client;
use Throwable;

/**
 * Client for UmweltInfo Statistics API.
 */
class UmweltInfoStatisticsApiDataClient implements DataClientInterface
{
  /**
   * @var Client
   */
  private Client $httpClient;

  /**
   * @param string $apiUrl
   */
  public function __construct(string $apiUrl)
  {
    $this->httpClient = new Client([
      'base_uri' => $apiUrl,
      'timeout'  => 1,
    ]);
  }

  /**
   * @inheritDoc
   */
  public function getStatisticsByPath(string $path): string
  {
    try {
      $response = $this->httpClient->request(
        'GET',
        '/stats' . $path,
        [
          'curl' => [
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_PRIOR_KNOWLEDGE
          ]
        ]
      );

      return $response->getBody()->getContents();
    } catch (Throwable $error) {
      Drupal::logger('umweltinfo_statistics')->error('Failed to query umwelt.info Statistics API: ' . $error->getMessage());

      return "{}";
    }
  }

  /**
   * @inheritDoc
   */
  public function addRatingByPath(string $path, int $rating): void
  {
    $this->httpClient->request(
      'POST',
      '/rating/' . $rating . $path,
      [
        'curl' => [
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_PRIOR_KNOWLEDGE
        ]
      ]
    );
  }
}
