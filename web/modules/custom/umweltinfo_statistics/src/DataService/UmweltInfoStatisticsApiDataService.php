<?php

namespace Drupal\umweltinfo_statistics\DataService;

use Drupal;
use Drupal\umweltinfo_statistics\Client\DataClientInterface;

/**
 * Data service for UmweltInfo Statistics API.
 */
class UmweltInfoStatisticsApiDataService implements DataServiceInterface
{
  /**
   * @var DataClientInterface
   */
  private DataClientInterface $dataClient;

  public function __construct()
  {
    $this->dataClient = Drupal::service('umweltinfo_statistics.data_client');
  }

  /**
   * @inheritDoc
   */
  public function getStatisticsByPath(string $path): string
  {
    return $this->dataClient->getStatisticsByPath($path);
  }

  /**
   * @inheritDoc
   */
  public function addRatingByPath(string $path, int $rating): void
  {
    $this->dataClient->addRatingByPath($path, $rating);
  }
}
