<?php

namespace Drupal\umweltinfo_statistics\DataService;

use GuzzleHttp\Exception\GuzzleException;

interface DataServiceInterface
{
  /**
   * @param string $path
   *
   * @return string
   */
  public function getStatisticsByPath(string $path): string;

  /**
   * @param string $path
   * @param int $rating
   *
   * @return void
   *
   * @throws GuzzleException
  */
  public function addRatingByPath(string $path, int $rating): void;
}
