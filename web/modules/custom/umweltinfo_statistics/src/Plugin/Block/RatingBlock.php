<?php

namespace Drupal\umweltinfo_statistics\Plugin\Block;

use Drupal;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\umweltinfo_statistics\DataService\DataServiceInterface;

/**
 * Provides a 'Rating' Block.
 */
#[Block(
  id: 'rating_block',
  admin_label: new TranslatableMarkup('Rating Block'),
  category: new TranslatableMarkup('Statistics Blocks')
)]
class RatingBlock extends BlockBase {
  /**
   * @var DataServiceInterface|mixed
   */
  private DataServiceInterface $dataService;

  /**
   * Cache max-age.
   *
   * @var int
   */
  protected int $cacheMaxAge = 3600;

  /**
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->dataService = \Drupal::service('umweltinfo_statistics.data_service');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int
  {
    return $this->cacheMaxAge;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    $stats = json_decode($this->dataService->getStatisticsByPath(Drupal::request()->getRequestUri()));

    $stars = $stats->rating?->stars ?? [];

    $rating = 0;
    $totalNumberOfVotes = 0;
    $totalRating = 0;
    foreach ($stars as $star => $numberOfVotes) {
      $totalNumberOfVotes += $numberOfVotes;
      $totalRating += $numberOfVotes * ($star + 1);
    }

    if ($totalNumberOfVotes > 0) {
      $rating = $totalRating / $totalNumberOfVotes;
    }

    return [
      '#theme' => 'rating_block',
      '#rating' => $rating,
    ];
  }
}
