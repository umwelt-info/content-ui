<?php

namespace Drupal\umweltinfo_statistics\Plugin\views\field;

use Drupal;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler for displaying usage statistics from UmweltInfo Statistics API.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("node_statistics")
 */
class NodeStatistics extends FieldPluginBase
{
  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field, since there's no such field in the database.
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $node = $this->getEntity($values);
    $path = $node->toUrl()->toString();

    $service = Drupal::service('umweltinfo_statistics.data_service');
    $statistics = $service->getStatisticsByPath($path);

    return $statistics;
  }
}
