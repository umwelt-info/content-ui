<?php

namespace Drupal\umweltinfo_statistics\Plugin\views\field;

use Drupal;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler for displaying percentage of mobile views from UmweltInfo Statistics API.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("node_percentage_mobile_views")
 */

class NodeMobileViewsPercentage extends FieldPluginBase
{
  /**
   * @{inheritdoc}
   */
  public function query()
  {
    // Leave empty to avoid a query on this field, since there's no such field in the database.
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values)
  {
    $node = $this->getEntity($values);
    $path = $node->toUrl()->toString();

    $service = Drupal::service('umweltinfo_statistics.data_service');
    $stats = json_decode($service->getStatisticsByPath($path));

    $accesses = $stats->view_duration->ten_seconds;
    $mobile_accesses = $stats->mobile_accesses;

    if (isset($accesses, $mobile_accesses) && $accesses > 0) {
      return round($mobile_accesses / $accesses * 100);
    } else {
      return "N/A";
    }
  }
}
