<?php

namespace Drupal\umweltinfo_statistics\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\umweltinfo_statistics\DataService\DataServiceInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * The controller to handle new Rating submissions.
 */
class RatingController extends ControllerBase {
  /**
   * @var DataServiceInterface|mixed
   */
  private DataServiceInterface $dataService;

  public function __construct()
  {
    $this->dataService = \Drupal::service('umweltinfo_statistics.data_service');
  }

  /**
   * Handle new Rating by Path.
   *
   * @param Request $request
   *
   * @return JsonResponse
   *
   * @throws GuzzleException
   */
  public function byPathAdd(Request $request): JsonResponse {
    $path = $request->request->get('path');
    $rating = $request->request->get('rating');

    $response = new JsonResponse([
      'error' => false,
      'message' => '',
      'code' => 200,
    ]);

    try {
      $this->dataService->addRatingByPath($path, (int) $rating);
    } catch (GuzzleException $e) {
      $response = new JsonResponse([
        'error' => true,
        'message' => $e->getMessage(),
        'code' => $e->getCode(),
      ], 500);
    }

    return $response;
  }
}
