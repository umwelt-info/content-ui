<?php

namespace Drupal\umweltinfo_api_blocks\DataService;

interface DataServiceInterface
{
  /**
   * @param int $numberOfThemes
   *
   * @return array
   */
  public function getTopNThemesData(int $numberOfThemes): array;

  /**
   * @return object
   */
  public function getRandomDatasetData(): object;

  /**
   * @return array
   */
  public function getDashboardAllData(): array;

  /**
   * @param string $topicsRoot
   *
   * @return array
   */
  public function getTopicsByTopicsRoot(string $topicsRoot): array;
}
