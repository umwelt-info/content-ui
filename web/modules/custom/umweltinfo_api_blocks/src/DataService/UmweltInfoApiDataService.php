<?php

namespace Drupal\umweltinfo_api_blocks\DataService;

use Drupal;
use Drupal\umweltinfo_api_blocks\Client\DataClientInterface;
use Throwable;

/**
 * Data service for UmweltInfo API.
 */
class UmweltInfoApiDataService implements DataServiceInterface
{
  /**
   * @var DataClientInterface
   */
  private DataClientInterface $dataClient;

  public function __construct()
  {
    $this->dataClient = Drupal::service('umweltinfo_api_blocks.data_client');
  }

  /**
   * @inheritDoc
   */
  public function getTopNThemesData(int $numberOfThemes): array
  {
    $data = $this->dataClient->getTopNThemes($numberOfThemes);

    $themes = [];
    foreach ($data as $datum) {
      try {
        $themes[] = [
          'id' => $datum->tag->Umthes->id,
          'label' => $datum->tag->Umthes->label,
        ];
      } catch (Throwable $e) {
        Drupal::logger('umweltinfo_api_blocks')
          ->error('There was an error getting during getting Themes data from UmweltInfo API: ' . $e->getMessage());
      }
    }

    return $themes;
  }

  /**
   * @inheritDoc
   */
  public function getRandomDatasetData(): object
  {
    return $this->dataClient->getRandomDataset();
  }

  /**
   * @inheritDoc
   */
  public function getDashboardAllData(): array
  {
    return $this->dataClient->getDashboardAllData();
  }

  /**
   * @inheritDoc
   */
  public function getTopicsByTopicsRoot(string $topicsRoot): array
  {
    return $this->dataClient->getTopicsByTopicsRoot($topicsRoot);
  }
}
