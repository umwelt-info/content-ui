<?php

namespace Drupal\umweltinfo_api_blocks\Client;

use Drupal;
use GuzzleHttp\Client;
use Throwable;

/**
 * Client for UmweltInfo API.
 */
class UmweltInfoApiDataClient implements DataClientInterface
{
  /**
   * @var string
   */
  private string $apiUrl;

  /**
   * @var Client
   */
  private Client $httpClient;

  /**
   * @param string $apiUrl
   */
  public function __construct(string $apiUrl)
  {
    $this->apiUrl = $apiUrl;
    $this->httpClient = new Client([
      'base_uri' => $this->apiUrl,
      'timeout'  => 2,
    ]);
  }

  /**
   * @inheritDoc
   */
  public function getTopNThemes(int $numberOfThemes): array
  {
    try {
      $data = $this->httpClient->get(
        '/top-n/tags',
        [
          'query' => [
            'n' => $numberOfThemes,
          ]
        ]
      );

      return json_decode($data->getBody()->getContents());
    } catch (Throwable $e) {
      Drupal::logger('umweltinfo_api_blocks')->error('There was an error getting data from UmweltInfo API: ' . $e->getMessage());

      return [];
    }
  }

  /**
   * @inheritDoc
   */
  public function getRandomDataset(): object
  {
    try {
      $data = $this->httpClient->get('/dataset/random');
      $processed_data = json_decode($data->getBody()->getContents());
      return $processed_data;
    } catch (Throwable $e) {
      Drupal::logger('umweltinfo_api_blocks')->error('There was an error getting data from UmweltInfo API: ' . $e->getMessage());

      return (object)[];
    }
  }

  /**
   * @inheritDoc
   */
  public function getDashboardAllData(): array
  {
    try {
      $data = $this->httpClient->get('/counts/all');

      return json_decode($data->getBody()->getContents());
    } catch (Throwable $e) {
      Drupal::logger('umweltinfo_api_blocks')->error('There was an error getting data from UmweltInfo API: ' . $e->getMessage());

      return [];
    }
  }

  /**
   * @inheritDoc
   */
  public function getTopicsByTopicsRoot(string $topicsRoot): array
  {
    try {
      $data = $this->httpClient->get(
        '/search',
        [
          'query' => [
            'topics_root' => $topicsRoot,
          ],
          'headers' => [
            'Accept' => 'application/json',
          ]
        ]
      );

      $result = json_decode($data->getBody()->getContents());

      return $result->topics;
    } catch (Throwable $e) {
      Drupal::logger('umweltinfo_api_blocks')->error('There was an error getting data from UmweltInfo API: ' . $e->getMessage());

      return [];
    }
  }
}
