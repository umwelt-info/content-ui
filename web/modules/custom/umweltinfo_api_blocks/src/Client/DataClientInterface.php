<?php

namespace Drupal\umweltinfo_api_blocks\Client;

interface DataClientInterface
{
  /**
   * Get Top N Themes items.
   *
   * @param int $numberOfThemes
   *
   * @return array
   */
  public function getTopNThemes(int $numberOfThemes): array;

  /**
   * Get a random dataset.
   *
   * @return object
   */
  public function getRandomDataset(): object;

  /**
   * Get all Dashboard data.
   *
   * @return array
   */
  public function getDashboardAllData(): array;

  /**
   * Get Topics by Topics Root from the Search.
   *
   * @param string $topicsRoot
   *
   * @return array
   */
  public function getTopicsByTopicsRoot(string $topicsRoot): array;
}
