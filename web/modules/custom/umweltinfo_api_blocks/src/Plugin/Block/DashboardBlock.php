<?php

namespace Drupal\umweltinfo_api_blocks\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\umweltinfo_api_blocks\DataService\DataServiceInterface;

/**
 * Provides a 'Dashboard' Block.
 */
#[Block(
  id: 'dashboard_block',
  admin_label: new TranslatableMarkup('Dashboard Block'),
  category: new TranslatableMarkup('API Blocks')
)]
class DashboardBlock extends BlockBase {
  /**
   * @var DataServiceInterface|mixed
   */
  private DataServiceInterface $dataService;

  /**
   * Cache max-age.
   *
   * @var int
   */
  protected int $cacheMaxAge = 3600;

  /**
   * @var array|string[]
   */
  protected array $chartsConfiguration = [
    'datasets_config' => 'Datasets',
    'sources_config' => 'Sources',
    'providers_config' => 'Providers',
  ];

  /**
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->dataService = \Drupal::service('umweltinfo_api_blocks.data_service');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int
  {
    return $this->cacheMaxAge;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    $data = $this->dataService->getDashboardAllData();

    return [
      '#theme' => 'dashboard_block',
      '#attached' => [
        'drupalSettings' => [
          'dashboardBlock' => [
            'data' => $data,
            'datasets_config' => $this->configuration['datasets_config'],
            'sources_config' => $this->configuration['sources_config'],
            'providers_config' => $this->configuration['providers_config'],
          ],
        ],
      ],
      '#title' => $this->configuration['title'],
      '#description' => $this->configuration['description'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array
  {
    return [
      'title' => 'Title',
      'description' => 'Description',
      'datasets_config' => [
        'labels' => [
          'de' => 'Datensätze',
          'en' => 'Datasets',
        ]
      ],
      'sources_config' => [
        'labels' => [
          'de' => 'Quellen',
          'en' => 'Sources',
        ]
      ],
      'providers_config' => [
        'labels' => [
          'de' => 'Anbieter',
          'en' => 'Providers',
        ]
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array
  {
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->configuration['title'],
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->configuration['description'],
    ];

    foreach ($this->chartsConfiguration as $key => $label) {
      $form[$key] = [
        '#type' => 'details',
        '#title' => $this->t($label . ' Configuration'),
        '#open' => false,
      ];

      $form[$key]['axis_limits'] = [
        '#type' => 'details',
        '#title' => $this->t('Axis Limits'),
        '#description' => $this->t('Leave empty to show all values. For dates use the same format as on graph - "31/12/2000".'),
        '#open' => false,
      ];

      $form[$key]['axis_limits']['y'] = [
        '#type' => 'details',
        '#title' => $this->t('Y-Axis (values)'),
        '#open' => false,
      ];

      $form[$key]['axis_limits']['y']['min'] = [
        '#type' => 'textfield',
        '#title' => 'Min',
        '#default_value' => $this->configuration[$key]['axis_limits']['y']['min'],
      ];

      $form[$key]['axis_limits']['y']['max'] = [
        '#type' => 'textfield',
        '#title' => 'Max',
        '#default_value' => $this->configuration[$key]['axis_limits']['y']['max'],
      ];

      $form[$key]['labels'] = [
        '#type' => 'details',
        '#title' => $this->t('Labels'),
        '#open' => false,
      ];

      $form[$key]['labels']['de'] = [
        '#type' => 'textfield',
        '#title' => 'German',
        '#default_value' => $this->configuration[$key]['labels']['de'],
      ];

      $form[$key]['labels']['en'] = [
        '#type' => 'textfield',
        '#title' => 'English',
        '#default_value' => $this->configuration[$key]['labels']['en'],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void
  {
    $values = $form_state->getValues();

    $this->configuration['title'] = $values['title'];
    $this->configuration['description'] = $values['description'];

    foreach ($this->chartsConfiguration as $key => $label) {
      $this->configuration[$key]['axis_limits']['y']['min'] = $values[$key]['axis_limits']['y']['min'];
      $this->configuration[$key]['axis_limits']['y']['max'] = $values[$key]['axis_limits']['y']['max'];
      $this->configuration[$key]['labels']['de'] = $values[$key]['labels']['de'];
      $this->configuration[$key]['labels']['en'] = $values[$key]['labels']['en'];
    }
  }
}
