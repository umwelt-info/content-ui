<?php

namespace Drupal\umweltinfo_api_blocks\Plugin\Block;

use Drupal;
use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\umweltinfo_api_blocks\DataService\DataServiceInterface;

/**
 * Provides a 'Taxonomy Term Topics' Block.
 */
#[Block(
  id: 'taxonomy_term_topics_block',
  admin_label: new TranslatableMarkup('Taxonomy Term Topics Block'),
  category: new TranslatableMarkup('API Blocks')
)]
class TaxonomyTermTopicsBlock extends BlockBase {
  /**
   * @var DataServiceInterface|mixed
   */
  private DataServiceInterface $dataService;

  /**
   * @var string
   */
  private string $baseSearchPath;

  /**
   * Cache max-age.
   *
   * @var int
   */
  protected int $cacheMaxAge = 3600;

  /**
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->dataService = \Drupal::service('umweltinfo_api_blocks.data_service');
    $this->baseSearchPath = '/suche';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int
  {
    return $this->cacheMaxAge;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    $term = \Drupal::request()->attributes->get('taxonomy_term');

    $resultTopics = [];
    if ($term) {
      $termId = $term->id();

      if ($termId) {
        $termFullPath = $this->getTaxonomyTermFullPath($termId);

        $topics = $this->dataService->getTopicsByTopicsRoot('/' . $termFullPath);
        foreach ($topics as &$topic) {
          $label = $topic[0];
          $label = str_replace($termFullPath, '', $label);
          $label = ltrim($label, '/');

          $path = ltrim($topic[0], '/');
          $path = str_replace('/', '\\', $path);

          $resultTopics[] = [
            'label' => $label,
            'path' => $path,
          ];
        }
      }
    }

    return [
      '#theme' => 'taxonomy_term_topics_block',
      '#headline' => $this->configuration['headline'],
      '#topics' => $resultTopics,
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array
  {
    return [
      'headline' => $this->t('Tags'),
    ];
  }

  /**
   * @param int $id
   *
   * @return string
   */
  private function getTaxonomyTermFullPath(int $id): string
  {
    $terms = Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadAllParents($id);

    $names = [];
    foreach ($terms as $term) {
      $names[] = $term->getName();
    }

    $names = array_reverse($names);

    return implode('/', $names);
  }
}
