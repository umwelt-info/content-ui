<?php

namespace Drupal\umweltinfo_api_blocks\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\umweltinfo_api_blocks\DataService\DataServiceInterface;

/**
 * Provides a 'Top 5' Block.
 */
#[Block(
  id: 'top_5_block',
  admin_label: new TranslatableMarkup('Top 5 Block'),
  category: new TranslatableMarkup('API Blocks')
)]
class Top5Block extends BlockBase {
  /**
   * @var DataServiceInterface|mixed
   */
  private DataServiceInterface $dataService;

  /**
   * @var string
   */
  private string $baseSearchPath;

  /**
   * Cache max-age.
   *
   * @var int
   */
  protected int $cacheMaxAge = 3600;

  /**
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->dataService = \Drupal::service('umweltinfo_api_blocks.data_service');
    $this->baseSearchPath = '/suche';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge(): int
  {
    return $this->cacheMaxAge;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array
  {
    $themeItems = $this->dataService->getTopNThemesData(5);
    foreach ($themeItems as &$themeItem) {
      $themeItem['url'] = $this->createLinkForTheme($themeItem['label']);
    }

    $randomItem = $this->dataService->getRandomDatasetData();
    $randomItem->{'url'} = $this->createLinkForContentItem($randomItem->{'source'}, $randomItem->{'id'});


    return [
      '#theme' => 'top_5_block',
      '#themes_title' => $this->configuration['themes_label'],
      '#random_title' => $this->configuration['random_label'],
      '#theme_items' => $themeItems,
      '#random_item' => $randomItem,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array
  {
    return [
      'themes_label' => 'The 5 most demanded topics',
      'random_label' => 'A random result from our search',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array
  {
    $form['themes_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Themes Label'),
      '#default_value' => $this->configuration['themes_label'],
    ];

    $form['random_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Random label'),
      '#default_value' => $this->configuration['random_label'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void
  {
    $values = $form_state->getValues();
    $this->configuration['themes_label'] = $values['themes_label'];
    $this->configuration['random_label'] = $values['random_label'];
  }

  /**
   * Generate URL to the search by theme.
   *
   * @todo Change the result URL after the implementation of filter values in query in search-ui app.
   *
   * @param string $themeLabel
   *
   * @return string
   */
  private function createLinkForTheme(string $themeLabel): string
  {
    return $this->baseSearchPath . '?tags=' . $themeLabel;
  }

  /**
   * Generate URL to the content detail page.
   *
   * @param string $source
   * @param string $id
   *
   * @return string
   */
  private function createLinkForContentItem(string $source, string $id): string
  {
    return $this->baseSearchPath . '?detail=' . urlencode($source . '/' . $id);
  }
}
