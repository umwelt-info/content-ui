<?php

$databases['default']['default'] = [
  'database' => './sites/default/files/.ht.sqlite',
  'prefix' => '',
  'driver' => 'sqlite',
  'namespace' => 'Drupal\\sqlite\\Driver\\Database\\sqlite',
  'autoload' => 'core/modules/sqlite/src/Driver/Database/sqlite/',
  'init_commands' => [
    'sync' => 'PRAGMA synchronous=NORMAL'
  ],
];

$settings['hash_salt'] = file_get_contents('/var/www/html/hash_salt.txt') ?: 'on a developer system';

$settings['config_sync_directory'] = '../config/sync';

$settings['trusted_host_patterns'] = [
  '^umwelt\.info$',
  '^.+\.umwelt.info$',
  '^localhost',
];

$settings['reverse_proxy'] = TRUE;
$settings['reverse_proxy_addresses'] = ['192.168.0.0/24'];
$settings['reverse_proxy_trusted_headers'] = \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_FOR | \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_PROTO | \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_HOST;

$settings['file_private_path'] = 'sites/default/files/private';

if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}
