<?php

/*
    vendor/bin/drush php:script --script-path=$PWD/scripts load_datenquellen -- </path/to/datenquellen.csv>
*/

set_error_handler(function (int $errno, string $errstr, string $errfile, int $errline) {
    throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
});

$field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions("node", "datenquelle");
$field_ebene_values = array_flip(options_allowed_values($field_definitions["field_ebene"]->getFieldStorageDefinition()));

$datenquellen = array();

$csv_file_path = $extra[0];

if (($csv_file = fopen($csv_file_path, "r")) !== FALSE) {
    $row_num = 0;

    while (($row = fgetcsv($csv_file, null, ";")) !== FALSE) {
        $row_num += 1;

        $title = $row[3];
        $institution = $row[1];

        if (empty($title) || empty($institution)) {
            $this->output()->writeln("Leere Datenquelle oder Institution in Eintrag {$row_num}.");
            continue;
        }

        $datenquellen["{$institution}/{$title}"] = [
            'title' => $title,
            'institution' => $institution,

            'ebene' => $field_ebene_values[$row[0]],
            'herausgeber_in' => $row[2],
        ];
    }

    fclose($csv_file);
}

$node_storage = \Drupal::entityTypeManager()->getStorage('node');

foreach ($datenquellen as $key => $datenquelle) {
    // Bezug zur Institution auflösen
    $query = \Drupal::entityQuery('node')
        ->accessCheck(false)
        ->condition('type', 'institution')
        ->condition('title', $datenquelle['institution']);

    $result = $query->execute();
    $institution_nid = array_shift($result);

    if (!$institution_nid) {
        die("Referenzierte Institution '{$datenquelle['institution']}' existiert nicht.");
    }

    // Prüfen ob Datenquelle bereits angelegt wurde
    $query = \Drupal::entityQuery('node')
        ->accessCheck(false)
        ->condition('type', 'datenquelle')
        ->condition('title', $datenquelle['title'])
        ->condition('field_institution', $institution_nid);

    $result = $query->execute();
    $nid = array_shift($result);

    if ($nid) {
        $this->output()->writeln("Datenquelle '{$datenquelle['title']}' existiert bereits als Node {$nid}.");
        continue;
    }

    $node = $node_storage->create([
        'type' => 'datenquelle',
        'title' => $datenquelle['title'],

        'field_institution' => $institution_nid,

        'field_ebene' => $datenquelle['ebene'],
        'field_herausgeber_in' => $datenquelle['herausgeber_in'],
    ]);

    $node->save();

    $this->output()->writeln("Datenquelle '{$datenquelle['title']}' als Node {$node->nid->value} angelegt.");
}
