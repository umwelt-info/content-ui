<?php

/*
    vendor/bin/drush php:script --script-path=$PWD/scripts migrate_ansprechpartner_in_kontakt
*/

set_error_handler(function (int $errno, string $errstr, string $errfile, int $errline) {
    throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
});

$query = \Drupal::entityQuery('node')
        ->accessCheck(false)
        ->condition('type', 'angebot');

$result = $query->execute();

$node_storage = \Drupal::entityTypeManager()->getStorage('node');

foreach ($result as $nid) {
    $node = $node_storage->load($nid);

    $node->field_ansprechpartner_in_kontakt = $node->field_ansprechpartner_in_e_mail;

    $node->save();
}
