<?php

/*
    vendor/bin/drush php:script --script-path=$PWD/scripts delete_ida
*/

$node_storage = \Drupal::entityTypeManager()->getStorage('node');

foreach(['institution', 'datenquelle', 'angebot'] as $type) {
    $entities = $node_storage->loadByProperties(["type" => $type]);
    $node_storage->delete($entities);
}
