<?php

/*
    vendor/bin/drush php:script --script-path=$PWD/scripts publish_ida
*/

$node_storage = \Drupal::entityTypeManager()->getStorage('node');

foreach(['institution', 'datenquelle', 'angebot'] as $type) {
    $query = \Drupal::entityQuery('node')
        ->accessCheck(false)
        ->condition('type', $type);

    $nids = $query->execute();

    $nodes = $node_storage->loadMultiple(array_values($nids));

    foreach ($nodes as $node) {
        $node->setPublished(true)->save();
    }
}
