<?php

/*
    vendor/bin/drush php:script --script-path=$PWD/scripts load_institutionen -- </path/to/datenquellen.csv>
*/

set_error_handler(function (int $errno, string $errstr, string $errfile, int $errline) {
    throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
});

$institutionen = array();

$csv_file_path = $extra[0];

if (($csv_file = fopen($csv_file_path, "r")) !== FALSE) {
    $row_num = 0;

    while (($row = fgetcsv($csv_file, null, ";")) !== FALSE) {
        $row_num += 1;

        $title = $row[1];

        if (empty($title)) {
            $this->output()->writeln("Leere Institution in Eintrag {$row_num}.");
            continue;
        }

        $institutionen[$title] = [];
    }

    fclose($csv_file);
}

$node_storage = \Drupal::entityTypeManager()->getStorage('node');

foreach ($institutionen as $title => $institution) {
    // Prüfen ob Institution bereits angelegt wurde
    $query = \Drupal::entityQuery('node')
        ->accessCheck(false)
        ->condition('type', 'institution')
        ->condition('title', $title);

    $result = $query->execute();
    $nid = array_shift($result);

    if ($nid) {
        $this->output()->writeln("Institution '{$title}' existiert bereits als Node {$nid}.");
        continue;
    }

    $node = $node_storage->create([
        'type' => 'institution',
        'title' => $title,
    ]);

    $node->save();

    $this->output()->writeln("Institution '{$title}' als Node {$node->nid->value} angelegt.");
}
