from pathlib import Path

import pandas as pd


dfs = []

for path in Path(".").glob("*_Datenquellen.xlsx"):
    print(path)

    df = pd.read_excel(path)

    dfs.append(df)

print(len(dfs))

df = pd.concat(dfs)

df.to_csv("datenquellen.csv", sep=";", header=False, index=False)
