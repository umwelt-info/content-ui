<?php

/*
    vendor/bin/drush php:script --script-path=$PWD/scripts verify_medium -- </path/to/abweichungen.csv> <base_url>
*/

$allowed_values = [
    'Website' => true,
    'Kartenanwendung' => true,
    'App' => true,
    'Datenbank' => true,
    'Interaktive Webanwendung' => true,
];

set_error_handler(function (int $errno, string $errstr, string $errfile, int $errline) {
    throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
});

$query = \Drupal::entityQuery('node')
        ->accessCheck(false)
        ->condition('type', 'angebot');

$result = $query->execute();

$node_storage = \Drupal::entityTypeManager()->getStorage('node');

$csv_file_path = $extra[0];
$base_url = $extra[1];

$csv_file = fopen($csv_file_path, 'w');
fputcsv($csv_file, ['Node-ID', 'Titel', 'Edit-Link'], ';');

foreach ($result as $nid) {
    $node = $node_storage->load($nid);

    if (!array_key_exists(trim($node->getTitle()), $allowed_values)) {
        fputcsv($csv_file, [$nid, $node->getTitle(), "${base_url}/de/node/${nid}/edit"], ';');
    }
}

fclose($csv_file);
