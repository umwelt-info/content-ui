<?php

/*
    vendor/bin/drush php:script --script-path=$PWD/scripts load_angebote -- </path/to/datenquellen.csv>
*/

set_error_handler(function (int $errno, string $errstr, string $errfile, int $errline) {
    throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
});

$field_definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions("node", "angebot");
$field_aktualisierungsrate_values = array_flip(options_allowed_values($field_definitions["field_aktualisierungsrate"]->getFieldStorageDefinition()));
$field_anbindung_values = array_flip(options_allowed_values($field_definitions["field_anbindung"]->getFieldStorageDefinition()));
$field_status_values = array_flip(options_allowed_values($field_definitions["field_status"]->getFieldStorageDefinition()));

$angebote = array();

$csv_file_path = $extra[0];

if (($csv_file = fopen($csv_file_path, "r")) !== FALSE) {
    $row_num = 0;

    while (($row = fgetcsv($csv_file, null, ";")) !== FALSE) {
        $row_num += 1;

        $title = $row[4];
        $institution = $row[1];
        $datenquelle = $row[3];

        if (empty($title) || empty($datenquelle)) {
            $this->output()->writeln("Leeres Angebot oder leere Datenquelle in Eintrag {$row_num}.");
            continue;
        }

        $angebote["{$institution}/{$datenquelle}/{$title}"] = [
            'title' => $title,
            'datenquelle' => $datenquelle,

            'verweis' => $row[5],
            'letzter_besuch' => $row[6],

            'ansprechpartner_in' => $row[7],
            'ansprechpartner_in_kontakt' => $row[8],
            'ansprechpartner_in_position' => $row[9],
            'anmerkungen' => $row[10],

            'api' => $row[11],
            'api_verweis' => $row[12],

            'status' => empty($row[13]) ? null : $field_status_values[$row[13]],
            'anbindung' => empty($row[15]) ? null : $field_anbindung_values[$row[15]],
            'aktualisierungsrate' => empty($row[16]) ? null : $field_aktualisierungsrate_values[$row[16]],
            'interne_anmerkungen' => $row[14],
        ];
    }

    fclose($csv_file);
}

$node_storage = \Drupal::entityTypeManager()->getStorage('node');

foreach ($angebote as $key => $angebot) {
    // Bezug zur Datenquelle auflösen
    $query = \Drupal::entityQuery('node')
        ->accessCheck(false)
        ->condition('type', 'datenquelle')
        ->condition('title', $angebot['datenquelle']);

    $result = $query->execute();
    $datenquelle_nid = array_shift($result);

    if (!$datenquelle_nid) {
        die("Referenzierte Datenquelle '{$angebot['datenquelle']}' existiert nicht.");
    }

    // Prüfen ob Angebot bereits angelegt wurde
    $query = \Drupal::entityQuery('node')
        ->accessCheck(false)
        ->condition('type', 'angebot')
        ->condition('title', $angebot['title'])
        ->condition('field_datenquelle', $datenquelle_nid);

    $result = $query->execute();
    $nid = array_shift($result);

    if ($nid) {
        $this->output()->writeln("Angebot '{$angebot['title']}' existiert bereits als Node {$nid}.");
        continue;
    }

    $node = $node_storage->create([
        'type' => 'angebot',
        'title' => $angebot['title'],

        'field_datenquelle' => $datenquelle_nid,

        'field_verweis' => $angebot['verweis'],
        'field_letzter_besuch' => $angebot['letzter_besuch'],

        'field_ansprechpartner_in' => $angebot['ansprechpartner_in'],
        'field_ansprechpartner_in_kontakt' => $angebot['ansprechpartner_in_kontakt'],
        'field_ansprechpartner_in_positio' => $angebot['ansprechpartner_in_position'],
        'field_anmerkungen' => $angebot['anmerkungen'],

        'field_api' => $angebot['api'],
        'field_api_verweis' => $angebot['api_verweis'],

        'field_status' => $angebot['status'],
        'field_anbindung' => $angebot['anbindung'],
        'field_aktualisierungsrate' => $angebot['aktualisierungsrate'],
        'field_interne_anmerkungen' => $angebot['interne_anmerkungen'],
    ]);

    $node->save();

    $this->output()->writeln("Angebot '{$angebot['title']}' als Node {$node->nid->value} angelegt.");
}
